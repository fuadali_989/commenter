<?php
/*
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2018 PrestaShop SA
 *  @version  Release: $Revision: 13573 $
 *  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminCommentController extends ModuleAdminController
{

    public function __construct()
    {
        parent::__construct();
        $this->bootstrap = true;
    }

    /**
     * AdminController::initContent() override
     * @see AdminController::initContent()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * AdminController::initContent() override
     * @see AdminController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        $this->context->smarty->assign(array(
            'comments' => Comment::getAll()
        ));
        $this->setTemplate('comment.tpl');
    }

    /*
     * Add jQuery library
     * Js file path
     * */

    public function setMedia()
    {
        parent::setMedia();
        $this->addJquery();
        $this->addJS(_MODULE_DIR_.'/commenter/views/js/commenter.js');
    }

    /*
     * get comments by product_id
     * send encoded json file
     * */
    public function ajaxProcessGetComments(){
        $product_id = Tools::getValue('product_id');
        $commentsByProduct = Comment::getUserDetailsByProductId($product_id);
        die(Tools::jsonEncode($commentsByProduct));
    }

    /*
     * approve comment by comment id
     */
    public function ajaxProcessApproveComment(){
        $comment_id = Tools::getValue('comment_id');
        $approvedComment = Comment::approveCommentByCommentId($comment_id);
        die(Tools::jsonEncode($approvedComment));
    }

    /*
     * disapprove comment by comment id
     */
    public function ajaxProcessDisapproveComment(){
        $comment_id = Tools::getValue('comment_id');
        $disapprovedComment = Comment::disapproveCommentByCommentId($comment_id);

        die(Tools::jsonEncode($disapprovedComment));
    }
}