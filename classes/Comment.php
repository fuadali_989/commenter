<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Comment extends ObjectModel
{
    public $id_comment;

    public $id_product;

    public $username;

    public $email;

    public $comment;

    public $date;

    public $condition;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'comment_module',
        'primary' => 'id_comment',
        'fields' => array(
            'id_product' =>        array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'username' =>                    array('type' => self::TYPE_STRING, 'size' => 32),
            'email' =>                array('type' => self::TYPE_STRING, 'size' => 32),
            'comment' =>                array('type' => self::TYPE_STRING, 'siez' => 512),
            'date' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'condition' =>                   array('type' => self::TYPE_STRING, 'size' => 40),
        ),
    );

    public static function getProductId()
    {
        $query = new DbQuery();
        $query->select('id_product');
        $query->from('comment_module', 'cm');

        return Db::getInstance()->execute($query);
    }

    public static function getAll()
    {
        return Db::getInstance()->executeS('
            SELECT 
        DISTINCT(`cm`.`id_product`) AS `product_id`,
        `cm`.`id_comment` AS `comment_id`,
        (SELECT COUNT(*) FROM `ps_comment_module` WHERE `condition`=\'NEW\' AND `id_product`=`id_product`) AS `new_comments_count`,
        (SELECT COUNT(*) FROM `ps_comment_module` WHERE `id_product`=`id_product`) AS `all_comments_count`,
        `pl`.`name` AS `name`
    FROM
        (`ps_comment_module` `cm`
        LEFT JOIN `ps_product_lang` `pl` ON (`pl`.`id_product` = `cm`.`id_product`))
        ');
    }

    public static function getAllUserComments()
    {
        $id_product = $_GET['id_product'];
        return Db::getInstance()->executeS("SELECT `username`, `email`, `comment` FROM `ps_comment_module` WHERE `condition` = 'approved' AND `id_product` = $id_product ");
    }

    public static function getUserDetailsByProductId($id)
    {
        return Db::getInstance()->executeS("SELECT `id_comment`, `username`, `email`, `comment` FROM `ps_comment_module` WHERE `id_product` = $id");
    }

    public static function disapproveCommentByCommentId($id)
    {
        return Db::getInstance()->execute("UPDATE `ps_comment_module` SET `condition` = 'DISAPPROVED' WHERE `id_comment` = $id");
    }

    public static function approveCommentByCommentId($id)
    {
        return Db::getInstance()->execute("UPDATE `ps_comment_module` SET `condition` = 'APPROVED' WHERE `id_comment` = $id");
    }
}
