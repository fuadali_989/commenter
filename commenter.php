<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2018 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */


if (!defined('_PS_VERSION_')) {
    exit;
}

require_once( _PS_MODULE_DIR_ . DIRECTORY_SEPARATOR . 'commenter' . DIRECTORY_SEPARATOR . 'classes' . DIRECTORY_SEPARATOR . 'Comment.php' );

class Commenter extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'commenter';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Internship';
        $this->need_instance = 1;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('İnternship Comment');
        $this->description = $this->l('İt is internship comment viewer');

        $this->confirmUninstall = $this->l('');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        Configuration::updateValue('COMMENTER_LIVE_MODE', true);

        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
                $this->registerHook('header') &&
                $this->registerHook('backOfficeHeader') &&
				$this->registerHook('displayRightColumnProduct') &&
                $this->createTabLink();
    }

    public function uninstall()
    {
        Configuration::deleteByName('COMMENTTEST_LIVE_MODE');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return $this->uninstallModuleTab() && parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitCommenterModule')) == true) {
            $this->postProcess();
        }

        return $this->renderForm();
    }

    public function uninstallModuleTab()
    {
        $id_tab = Tab::getIdFromClassName('AdminComment');
        if($id_tab)
        {
            $tab = new Tab($id_tab);
            return $tab->delete();
        }

        return true;
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitCommenterModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Live mode'),
                        'name' => 'COMMENTER_LIVE_MODE',
                        'is_bool' => true,
                        'desc' => $this->l('Use this module in live mode'),
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'COMMENTER_LIVE_MODE' => ConfigurationCore::get('COMMENTER_LIVE_MODE', ConfigurationCore::get('COMMENTER_LIVE_MODE', true)),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        ConfigurationCore::updateValue('COMMENTER_LIVE_MODE', Tools::getValue('COMMENTER_LIVE_MODE'));

        if (ConfigurationCore::get('COMMENTER_LIVE_MODE') == "1") {
            return $this->registerHook('displayRightColumnProduct');
        } else if (ConfigurationCore::get('COMMENTER_LIVE_MODE') == "0"){
            return $this->unregisterHook('displayRightColumnProduct');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }
	
	//Hook method for displaying comment part in FO product page
	public function hookDisplayRightColumnProduct() 
	{
        if(Tools::getIsset('sendComment')) {
            $username = Tools::getValue('addComment_Name');
            $email = Tools::getValue('addComment_Email');
            $comment = Tools::getValue('addComment_Comment');
            $date = date("Y-m-d H:i:s");
            $id_product = $_GET['id_product'];

            $this->assignError();

            Db::getInstance()->execute("INSERT INTO `ps_comment_module` (`id_product`,`username`,`email`, `comment`,
            `condition`, `date`) values('$id_product', '$username', '$email', '$comment', 'new', '$date')");
        }

        $this->context->smarty->assign(array(
            'comments' => Comment::getAllUserComments()
        ));

        return $this->display(__FILE__, 'views/templates/hook/ui-form.tpl');

	}

     public function assignError() 
    {
        $username = Tools::getValue('addComment_Name');
        $email = Tools::getValue('addComment_Email');
        $comment = Tools::getValue('addComment_Comment');

        if(empty($username) || empty($email) || empty($comment))
        {
            $this->context->smarty->assign('confirmation', 'Please fill the empty fields');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addCSS(array(
			$this->_path.'views/css/addcomment.css'
		));

		$this->context->controller->addJS(array(
			$this->_path.'views/js/addcomment.js'
		));
    }

    public function createTabLink()
    {
        $tab = new Tab();
        foreach(Language::getLanguages() as $lang)
        {
            $tab->name[$lang['id_lang']] = $this->l('Comment');
        }
        $tab->class_name = 'AdminComment';
        $tab->module = $this->name;
        $tab->id_parent = 0;
        $tab->save();
        return true;
    }


}