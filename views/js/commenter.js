

$(document).ready(function () {


    $('#viewComments').click(function () {
        return $.ajax({
            url: currentIndex + '&token=' + token,
            data: {
                action: 'getcomments',
                ajax: true,
                token: token,
                product_id: $('#product_id').html(),
            },
            method: 'POST',
            success: function (data) {
                // output for creating table
                let output = '';
                $('#commentList').empty();
                let object_data = JSON.parse(data);
                console.log(object_data);
                for (let user in object_data) {
                    output += `
                        <tr>
                            <td>${object_data[user].username}</td>
                            <td>${object_data[user].email}</td>
                            <td>${object_data[user].comment}</td>
                            <td>
                                <button class="btn btn-xs btn-success" onclick="approveComment(${object_data[user].id_comment})" id="app${object_data[user].id_comment}">Approve</button>
                                <button class="btn btn-xs btn-danger" onclick="disapproveComment(${object_data[user].id_comment})" id="dis${object_data[user].id_comment}">Disapprove</button>
                            </td>
                        </tr>
                    `;
                }


                $('#commentList').append(output)
            }
        })

    })

})

// ajax method with comment id
// sends controller comment id
function approveComment(id_comment) {
    $.ajax({
        url: currentIndex + '&token=' + token,
        url: 'http://presta16.test/admin1152ptvrm/index.php?controller=AdminComment&token=c1f3cfa74e0999250293789feb1ff6c5',

        url: currentIndex + '&token=' + token,
        data: {
            action: 'approvecomment',
            ajax: true,
            comment_id: id_comment,
        },
        method: 'POST',
        success: function (data) {
            // get buttons ids
            let approveComment = document.getElementById('app'+id_comment);
            let disapproveComment = document.getElementById('dis'+id_comment);
            if(data === 'true'){
                approveComment.setAttribute('disabled',true);
                disapproveComment.removeAttribute('disabled');
            }
        }
    })
}

// ajax method with comment id
// sends controller comment id
function disapproveComment(id_comment) {
    $.ajax({

        url: currentIndex + '&token=' + token,

        url: 'http://presta16.test/admin1152ptvrm/index.php?controller=AdminComment&token=c1f3cfa74e0999250293789feb1ff6c5',

        url: currentIndex + '&token=' + token,

        data: {
            action: 'disapprovecomment',
            ajax: true,
            comment_id: id_comment,
        },
        method: 'POST',
        success: function (data) {
            let approveComment = document.getElementById('app'+id_comment);
            let disapproveComment = document.getElementById('dis'+id_comment);
            if(data == 'true'){
                disapproveComment.setAttribute('disabled',true);
                approveComment.removeAttribute('disabled');
            }
        }
    })
}

