<div class="panel-body">
    <div class="col-lg-10">
        <table class="table table-hovered">
            <thead>
            <tr>
                <th>{l s="Product Id" mod="bestcomment"}</th>
                <th>{l s="Product Name" mod="bestcomment"}</th>
                <th>{l s="New Comments" mod="bestcomment"}</th>
                <th>{l s="All Comments" mod="bestcomment"}</th>
                <th>{l s="#" mod="bestcomment"}</th>
            </tr>
            </thead>
            <tbody id="category_products">
            {foreach from=$comments item=comment}
                <tr>
                    <td id="product_id">{$comment['product_id']}</td>
                    <td>{$comment['name']}</td>
                    <td>{$comment['all_comments_count']}</td>
                    <td>{$comment['new_comments_count']}</td>
                    <td>
                        <button type="button" id="viewComments" class="btn btn-primary"
                                data-toggle="modal" data-target="#commentListModal">
                            View Comments
                        </button>
                    </td>
                </tr>
            {/foreach}
            </tbody>
        </table>
    </div>
    <div class="clearfix"></div>
    <div>
        <div class="modal fade" id="commentListModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="table table-condensed">
                            <thead>
                            {*<td>Id</td>*}
                            <td>Username</td>
                            <td>Email</td>
                            <td>Comment</td>
                            <td>Actions</td>
                            </thead>
                            <tbody id="commentList">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>