{if isset($confirmation)}
	<div class="alert alert-danger">Empty fields must be filled</div>
{/if}
<h2 class="comment-title">Add your comment</h2>
<form method="post">
	<div class="form-group">
		<label class="addcomment-label" for="exampleFormControlInput1">Name*</label>
		<input type="text" class="form-control addcomment-input" id="exampleFormControlInput1" name="addComment_Name" placeholder="Type your name...">
	</div>

	<div class="form-group">
		<label class="addcomment-label" for="exampleFormControlInput1">Email address*</label>
		<input type="email" class="form-control addcomment-input" id="exampleFormControlInput2" name="addComment_Email" placeholder="Type your email...">
	</div>


	<div class="form-group">
		<label class="addcomment-label" for="exampleFormControlTextarea1">Your Comment*</label>
		<textarea class="form-control addcomment-input" id="exampleFormControlTextarea1" rows="3" name="addComment_Comment" placeholder="Type Your comment here..."></textarea>
	</div>

	<div class="form-group text-right">
		<input type="submit" class="addcomment-submitButton" id="exampleFormControlInput2" value="Submit" name="sendComment">
	</div>
</form>


<h2 class="comment-title">Comments</h2>

{foreach from=$comments item=comment}
<div class="comment-box">
	<div class="comment-box-left">
		<h3 class="comment-username">{$comment['username']}</h3>
		<h3 class="comment-email">{$comment['email']}</h3>
	</div>
	<div class="comment-box-right">
		<h3 class="comment-comment">{$comment['comment']}</h3>
	</div>
</div>
{/foreach}
